package org.vishwas.creditCheck.model;

import java.util.List;

public class UserEligibilityDetails {
	
	private String firstName;
	private String lastName;
	private double loanAmount;
	private int outstandingBalance;
	private String eligibilityStatus;
	private String userExists;
	private List<Loan> loanHistory;
	
	
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public List<Loan> getLoanHistory() {
		return loanHistory;
	}
	public void setLoanHistory(List<Loan> loanHistory) {
		this.loanHistory = loanHistory;
	}
	public String getUserExists() {
		return userExists;
	}
	public void setUserExists(String userExists) {
		this.userExists = userExists;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getOutstandingBalance() {
		return outstandingBalance;
	}
	public void setOutstandingBalance(int outstandingBalance) {
		this.outstandingBalance = outstandingBalance;
	}
}
