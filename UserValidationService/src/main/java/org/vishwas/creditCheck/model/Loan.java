package org.vishwas.creditCheck.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Loan {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int loanId;
	private String loanExists;
	private Date loanInitiationDate;
	private Date loanClearedDate;
	private float loanAmount;
	@ManyToOne
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public String getLoanExists() {
		return loanExists;
	}
	public void setLoanExists(String loanExists) {
		this.loanExists = loanExists;
	}
	public Date getLoanInitiationDate() {
		return loanInitiationDate;
	}
	public void setLoanInitiationDate(Date loanInitiationDate) {
		this.loanInitiationDate = loanInitiationDate;
	}
	public Date getLoanClearedDate() {
		return loanClearedDate;
	}
	public void setLoanClearedDate(Date loanClearedDate) {
		this.loanClearedDate = loanClearedDate;
	}
	public float getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(float loanAmount) {
		this.loanAmount = loanAmount;
	}
	@Override
	public String toString() {
		return "Loan [loanId=" + loanId + ", loanExists=" + loanExists + ", loanInitiationDate=" + loanInitiationDate
				+ ", loanClearedDate=" + loanClearedDate + ", loanAmount=" + loanAmount + ", user=" + user + "]";
	}
}
