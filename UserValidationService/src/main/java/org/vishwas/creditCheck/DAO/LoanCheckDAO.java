package org.vishwas.creditCheck.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.vishwas.creditCheck.model.Loan;

@Repository
public interface LoanCheckDAO extends CrudRepository<Loan,Integer>{
	@Query(value="select * from loan a where a.user_user_id=:user_id",nativeQuery = true)
	public List<Loan> findLoanHistoryByUserId(@Param("user_id")int userId);
}
